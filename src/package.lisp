;;;; package.lisp

(defpackage #:dmomd
  (:local-nicknames (:tgk #:trivial-gamekit)
                    (:a #:alexandria)
                    (:dungen #:net.mfiano.lisp.dungen))
  (:use #:cl)
  (:export #:start
           #:stop
           #:restart))
