(in-package #:dmomd)

(defvar *animation-credits-player-won* nil)

;;;; parts of this code are from Borodusts game "Notalone"
;;;; parts of this code are Elias Feijós game "Deserted"

(defstruct keyframe
  (image nil :read-only t)
  ;; offset from the animations position from the
  (origin (tgk:vec2 0 0) :type tgk:vec2 :read-only t)

  (time 0 :type single-float :read-only t)
  (flipped-x nil :type boolean :read-only t)
  (flipped-y nil :type boolean :read-only t))


(defclass animation ()
  ((name :initarg :name :initform "<no-name>" :accessor animation-name)
   (sequence :initarg :sequence :initform nil)
   (looped-p :initarg :looped-p :initform nil :reader animation-looped-p)
   (start-time :initform 0 :accessor start-time-of)
   (started-p :initform 0 :accessor started-p)
   (animation-length
    :initarg :animation-length
    :initform 0
    :reader animation-length-of)
   (position
    :initarg :position
    :initform (tgk:vec2 0 0)
    :accessor position-of)))


(defun make-animation (name sequence animation-length &key looped-p (position (tgk:vec2 0 0)))
  (let ((frames (loop :for (image time origin flipped-x flipped-y)
                   :in (sort sequence #'< :key #'second)
                   :collect (make-keyframe :image image
                                           :time (bodge-util:f time)
                                           :origin origin
                                           :flipped-x flipped-x
                                           :flipped-y flipped-y))))
    (make-instance 'animation
                   :name name
                   :position position
                   :looped-p looped-p
                   :animation-length animation-length
                   :sequence (make-array (length frames)
                                         :element-type 'keyframe
                                         :initial-contents frames))))

(defmethod print-object ((a animation) out)
  (print-unreadable-object (a out)
    (format out "animation-~A" (animation-name a))))


(defun animation-finished-p (animation current-time)
  (if (animation-looped-p animation)  ; looping animations are never finished
      (return-from animation-finished-p nil))
  (with-slots (start-time animation-length) animation
    (if (or
         (eql 0 animation-length)
         (>= (- current-time start-time) animation-length))
        t)))


(defun start-animation (animation current-time &key (position nil))
  (unless (null animation)
   "`position' defines where to put the animations origin."
   (when position
     (setf (position-of animation) position))
   (with-slots (started-p start-time)
       animation
     (setf started-p t)
     (setf start-time current-time))))


(defun get-looped-time (animation time)
  (let ((animation-length (animation-length-of animation)))
    (if (= animation-length 0)
        0
        (mod time animation-length))))


(defun get-frame (animation current-time)
  (with-slots (sequence start-time looped-p) animation
    (let* ((time-delta (- current-time start-time))
           (animation-timestamp (if looped-p (get-looped-time animation time-delta) time-delta)))
      (multiple-value-bind (result idx)
          (bodge-util:search-sorted animation-timestamp sequence :test #'= :key #'keyframe-time)
        (or result (aref sequence (max (1- idx) 0)))))))

(defun create-all-animations ()
  ;; animations are defined during runtime,
  ;; because we have to wait for all resources to be loaded first
  (create-animations-player)
  (create-animations-slime)
  (create-animations-rat)
  ;; (create-animations 'zombie)
  (create-animations-demon))
