(in-package #:dmomd)


(defun create-edge-colors (num-edges)
  (setf *edge-colors* (make-hash-table))
  (dotimes (i num-edges)
    (setf (gethash i *edge-colors*)
          (random-light-color))))

(defun clear-screen (&key (color (tgk:vec4 0 0 0 1)))
  (tgk:draw-rect (tgk:vec2 0 0)
                 (elt *internal-screen-size* 0)
                 (elt *internal-screen-size* 1)
                 :fill-paint color))

(defun show-regions (grid)
  (let ((camera-pos (vector->vec2 (camera-bottom-left *dungeon-camera*)))
        (text-offset (tgk:vec2 (- *dungeon-cell-width*  12)
                               (- *dungeon-cell-height* 14)))
        (color (tgk:vec4 .8 .8 .8 1)))
    (2d-loop-over (grid x y)
      (let ((region-id (dungen::cell-region (aref *grid* x y))))
        (unless (zerop region-id)
         (draw-text (format nil "~A" region-id)
                    (tgk:subt
                     (tgk:vec2 (* x *dungeon-cell-width*)
                               (* y *dungeon-cell-height*))
                     camera-pos)
                    :fill-color color
                    :font (font :quikhand 20)))))))

(defun draw-debug-overlay ()
  (draw-string-list
   (tgk:vec2 10 130)
   20
   (tgk:vec4 1 1 1 1)
   (font :quikhand 20)
   20
   (list (format nil "peaceful: ~A" *peaceful*)
         (format nil "show-aggression-tiles: ~A" *show-aggression-tiles*)
         (format nil "player invincible: ~A" *player-invincible*)
         (format nil "enemies not killable: ~A" *player-can-not-kill-enemy*)
         (format nil "Level: ~A" (level *player*))
         (format nil "Experience: (~A/~A)" (xp *player*) (xp-threshold *player*))
         (format nil "Total Experience: ~A" (xp-total *player*)))))

(defun draw-poly-edges (&optional (edge-pool *edge-pool*))
  (unless (or (zerop (hash-table-count edge-pool))
              (null edge-pool))
    (maphash
     (lambda (k v)
       (with-accessors ((sx sx) (ex ex) (sy sy) (ey ey))
           v
         (let ((start-pos (vector->vec2
                           (position-inside-camera
                            *dungeon-camera*
                            (cell-position->pixel/bottom-left (vector sx sy)))))
               (end-pos (vector->vec2
                         (position-inside-camera
                          *dungeon-camera*
                          (cell-position->pixel/bottom-left (vector ex ey)))))
               (color (gethash k *edge-colors*))
               ;; (color (random-color))
               )
           (gamekit:draw-line start-pos end-pos color :thickness 1.5)
           (draw-text (format nil "~A" k) (gamekit:div (gamekit:add end-pos start-pos) 2)
                      :fill-color color
                      :font (font :quikhand 30)))))
     edge-pool)))

(defun gray-shade (&key (value 0)  (opacity 1))
  "Value and opacity need to be between 0 and 1."
  (tgk:vec4 value value value opacity))

(defun draw-dungeon-cell-sprite (cell-coordinates)
  "Draws the appropriate sprite"
  (let ((draw-pos (vector->vec2
                   (position-inside-camera
                    *dungeon-camera*
                    (cell-position->pixel/bottom-left cell-coordinates))))
        (cell (aref *grid* (elt cell-coordinates 0) (elt cell-coordinates 1)))
        (cell-features (cell-features cell-coordinates)))
    ;; sanity check
    (when (cell-outside-grid-p cell-coordinates)
      ;; when the index is out of bounds, the camera sees tiles outside of our map
      ;; we'll draw these tiles as walls
      ;; (format t "index out of bounds in #'draw-dungeon-cell-sprite, when called with arg `~A'." cell-coordinates)
      (draw-sprite (get-resource '(:img :dungeon :wall-tile))
                   draw-pos)
      (return-from draw-dungeon-cell-sprite nil))

    (cond ((and (cell-has-door/horizontal-p cell)
                (hellfire-door-p cell))
           (draw-sprite (get-resource '(:img :dungeon :hellfire-door-h-tile)) draw-pos))
          ;;------------------------------------------------------------
          ((and (cell-has-door/vertical-p cell)
                (hellfire-door-p cell))
           (draw-sprite (get-resource '(:img :dungeon :hellfire-door-v-tile)) draw-pos))
          ;;------------------------------------------------------------
          ((cell-wall-p cell)
           (draw-sprite (get-resource '(:img :dungeon :wall-tile)) draw-pos))
          ;;------------------------------------------------------------
          ((eql (car cell-features) :door/horizontal)
           (draw-sprite (get-resource '(:img :dungeon :door-h-tile)) draw-pos))
          ;;------------------------------------------------------------
          ((eql (car cell-features) :door/vertical)
           (draw-sprite (get-resource '(:img :dungeon :door-v-tile)) draw-pos))
          ;;------------------------------------------------------------
          (T (draw-sprite (get-resource '(:img :dungeon :floor-tile)) draw-pos)))))

(defun draw-grid (&optional (color (gray-shade :value 0 :opacity 1)))
  (let ((cs-x (elt *dungeon-cell-size* 0))
        (cs-y (elt *dungeon-cell-size* 1))
        (ww (elt *window-size* 0))
        (wh (elt *window-size* 1)))
    (labels ((draw-tile-marker (pos)
               (tgk:draw-rect pos
                              (* cs-x *scale-factor*) (* cs-y *scale-factor*)
                              :fill-paint nil
                              :stroke-paint color
                              :thickness 1)))
      (let* ((w-times (1+ (/ ww cs-x *scale-factor*)))
             (h-times (1+ (/ wh cs-y *scale-factor*))))
        (do ((w-idx 0 (1+ w-idx))
             (h-idx 0))
            ((>= h-idx h-times) T)
          (when (>= w-idx w-times)
            (setf w-idx (mod w-idx w-times))
            (incf h-idx))
          ;; here we add the cameras offset of 32x8
          (let ((x-pos (- (* w-idx cs-x *scale-factor*) 32))
                (y-pos (- (* h-idx cs-y *scale-factor*) 56)))
            (draw-tile-marker (tgk:vec2 x-pos y-pos))))))))

(defun draw-player-health-bar (&key
                        (curr-hp (hp *player*))
                        (max-hp  (hp-max *player*)))
  (let ((draw-pos (gamekit:subt *window-top-left-corner*
                                (gamekit:vec2 -50 75)))
        (hp-ratio (/ curr-hp max-hp))
        (max-hp-scale-factor (/ 150 max-hp))
        (hp-scale-factor (if (zerop curr-hp) 0 (/ 150 curr-hp)))
        (height 30))
    (gamekit:draw-rect draw-pos
                       (* max-hp  max-hp-scale-factor)
                       height
                       :fill-paint (gamekit:vec4 .5 0 0 1))
    (gamekit:draw-rect draw-pos
                       (* (* curr-hp hp-scale-factor) hp-ratio)
                       height
                       :fill-paint (gamekit:vec4 .75 0 0 1))
    (draw-image (gamekit:subt draw-pos
                              (tgk:vec2 21 7.5))
                :img-ui-life-bar)))

(defun draw-player-mana-bar (&key
                        (curr-mp (mp *player*))
                        (max-mp  (mp-max *player*)))
  (let ((draw-pos (gamekit:subt *window-top-left-corner*
                                (gamekit:vec2 -50 150)))
        (mp-ratio (/ curr-mp max-mp))
        (max-mp-scale-factor (/ 150 max-mp))
        (mp-scale-factor (if (zerop curr-mp) 0 (/ 150 curr-mp)))
        (height 30))
    (gamekit:draw-rect draw-pos
                       (* max-mp  max-mp-scale-factor)
                       height
                       :fill-paint (gamekit:vec4 0 .1 .5 1))
    (gamekit:draw-rect draw-pos
                       (* (* curr-mp mp-scale-factor) mp-ratio)
                       height
                       :fill-paint (gamekit:vec4 0 .2 1 1))
    (draw-image (gamekit:subt draw-pos
                              (tgk:vec2 21 7.5))
                :img-ui-life-bar)))

(defun draw-player-xp-bar (&key (xp (xp *player*)) (xp-threshold  (xp-threshold *player*)))
  (let* ((draw-pos (gamekit:subt *window-top-left-corner* (gamekit:vec2 -40 200)))
         (xp-ratio (/ xp xp-threshold))
         (bar-length 175)
         (xp-threshold-scale-factor (/ bar-length xp-threshold))
         (xp-scale-factor (if (zerop xp) 0 (/ bar-length xp)))
         (height 10))
    (gamekit:draw-rect draw-pos
                       (* xp-threshold  xp-threshold-scale-factor)
                       height
                       :stroke-paint (gamekit:vec4 .5 .45 .1 1)
                       :thickness 2
                       :fill-paint (gamekit:vec4 .35 .25 .05 1))
    (gamekit:draw-rect draw-pos
                       (* (* xp xp-scale-factor) xp-ratio)
                       height
                       :fill-paint (gamekit:vec4 1 .8 .2 .4))
    ;; (draw-text (format nil "Level: ~A" (level *player*))
    ;;            (gamekit:subt *window-top-left-corner* (gamekit:vec2 -50 200))
    ;;            :fill-color (gamekit:vec4 1 .8 .2 1)
    ;;            :font (font :quikhand 30))
    ;; (draw-image (gamekit:subt draw-pos
    ;;                           (tgk:vec2 21 7.5))
    ;;             :img-ui-life-bar)
    ))

(defun draw-player-bars ()
  "Draws the players health, mana and experience bar."
  (if *player-invincible*
      (progn
        (ge.vg:with-alpha (.25)
          (draw-player-health-bar)
          (draw-player-mana-bar)
          (draw-player-xp-bar)))
      (progn
        (draw-player-health-bar)
        (draw-player-mana-bar)
        (draw-player-xp-bar))))


(defmethod draw-item ((item item))
  "Draws the item at the correct pixel position.
If the item has a sprite, this sprite will be drawn.
Otherwise it will have a color instead and that color will be drawn."
  (let ((pos (vector->vec2
              (position-inside-camera
               *dungeon-camera*
               (cell-position->pixel/bottom-left (dungeon-pos item)))))
        (pos-center (vector->vec2
                     (position-inside-camera
                      *dungeon-camera*
                      (cell-position->pixel/center (dungeon-pos item)))))
        (image (item-image item)))
    (if (subtypep (type-of image) 'tgk:vec4) ; draw the color if it doesn't have a sprite
        (tgk:draw-rect pos
                       (elt *dungeon-cell-size* 0)
                       (elt *dungeon-cell-size* 1)
                       :fill-paint (item-image item))
        (if (chest-p item)
            (progn
              (draw-sprite (item-image item) ; draw sprite sprite if available
                           pos)
              (ge.vg:with-alpha (0.05)
                (gamekit:draw-circle
                 pos-center
                 *dungeon-cell-width*
                 :fill-paint (gamekit:vec4 1 1 .3 1))))
            (draw-sprite (item-image item) ; draw sprite sprite if available
                         pos)))))

(defun draw-effects (effect-animation-list)
  (dolist (a effect-animation-list)

    (let* ((frame (get-frame a (now)))
           (origin (keyframe-origin frame))
           (flipped-x (keyframe-flipped-x frame))
           (flipped-y (keyframe-flipped-y frame))
           (position (position-of a)))
      (tgk:with-pushed-canvas ()
        (draw-sprite (keyframe-image frame)
                     position))
      ;; after the animation has finished, change the current-animation to be the default
      (when (animation-finished-p a (now))
        (setf *effect-animations* (delete a *effect-animations* :test #'equalp))))))

(defun draw-thing (position color)
  (draw-cell position color))

(defun draw-masked-cells (origin mask color)
  (dolist (pos (relatives-to-absolutes origin (mask-to-relative mask)))
    (draw-cell pos color)))

(defun draw-cell (cell-position color)
  (tgk:draw-rect (vector->vec2
                  (position-inside-camera *dungeon-camera*
                                          (cell-position->pixel/bottom-left cell-position)))
                 (elt *dungeon-cell-size* 0)
                 (elt *dungeon-cell-size* 1)
                 :fill-paint color))

(defun draw-dungeon-sprite (cell-position image)
  (draw-sprite image
               (vector->vec2
                (position-inside-camera *dungeon-camera*
                                        (cell-position->pixel/bottom-left cell-position)))))

(defmethod draw-vision-circle ((p player))
  (with-slots ((r vision-radius))
      p
    (gamekit:draw-circle (vector->vec2
                          (position-inside-camera
                           *dungeon-camera*
                           (cell-position->pixel/center
                            (dungeon-pos p))))
                         (* r *dungeon-cell-width*)
                         :stroke-paint +color-antiquewhite2+
                         )))

(defun draw-battle-enemies (&optional (battle-enemies *battle-enemies*))
  (dolist (enemy battle-enemies)
    (draw enemy (battle-draw-pos enemy))
    (if *player-can-not-kill-enemy*
        (ge.vg:with-alpha (.25)
          (draw-enemy-health-bar enemy :draw-pos (battle-draw-pos enemy)))
        (draw-enemy-health-bar enemy :draw-pos (battle-draw-pos enemy)))))


(defun draw-string-list (position gap color font font-height str-list &optional (active -1))
  (%draw-string-list position gap color font font-height str-list 0 active))

(defun %draw-string-list (position gap color font font-height str-list i active)
  (if (null str-list)
      'DONE
      (let* ((x-pos (tgk:x (eval position)))
             (y-pos (tgk:y (eval position)))
             (new-y-pos (- y-pos (* i gap)))
             (text (first str-list)))
        (tgk:with-pushed-canvas ()
          (tgk:translate-canvas x-pos new-y-pos)
          (if (= active i)
              (progn
                (tgk:translate-canvas 10 0)
                (draw-text text
                           +zerop-pos+
                           :fill-color (tgk:vec4 1 0 0 1)
                           :font font)
                (tgk:scale-canvas (* (/ (length text) 100) (/ font-height 2)) 0))
              ;; else
              (draw-text text
                         +zerop-pos+
                         :fill-color color
                         :font font)))
        ;; recursive call
        (%draw-string-list position gap color font font-height (rest str-list) (1+ i) active))))

(defun draw-player-information-menu ()
  (ge.vg:with-alpha (.6)
    (tgk:draw-rect (tgk:add *window-center*
                            (tgk:vec2 130 -350))
                   500 700
                   :fill-paint (hexcolor "#1C1411")
                   :thickness 10
                   :stroke-paint (hexcolor "#2E1F1C")))
  ;;--------------------------------------------------------------------------------
  (let* ((regular-color (hexcolor "#AE8B50"))
         (highlight-color (hexcolor "#EDAC3F"))
         (big-font (font :quikhand 32))
         (small-font-size 22)
         (small-font (font :quikhand small-font-size)))
   (draw-text "Decent Magician:"
              (tgk:add *window-top-right-corner*
                       (tgk:vec2 -495 -50))
              :fill-color highlight-color
              :font big-font)
   (draw-string-list (tgk:add *window-top-right-corner* (tgk:vec2 -495 -80))
                     30
                     regular-color
                     small-font
                     small-font-size
                     (list
                      (format nil "Level: ~A (~A/~A)  -  Total XP: ~A"
                              (level *player*)
                              (xp *player*)
                              (xp-threshold *player*)
                              (xp-total *player*))
                      (format nil "Health: (~A/~A)" (hp *player*) (hp-max *player*))
                      (format nil "Mana: (~A/~A)" (mp *player*) (mp-max *player*))
                      (format nil "Strength: ~A" (strength *player*))
                      (format nil "agility: ~A" (agility *player*))
                      (format nil "Intelligence: ~A" (intelligence *player*))
                      (format nil "Initiative: ~A" (initiative *player*))
                      (format nil "Physical Defense: ~A" (physical-defense *player*))
                      (format nil "Vulnerabilities: ~{~A~^, ~}"
                              (mapcar (lambda (s) (string-capitalize (string-downcase s)))
                                      (vulnerabilities *player*)))
                      (format nil "Resistances: ~{~A~^, ~}"
                              (mapcar (lambda (s) (string-capitalize (string-downcase s)))
                                      (resistances *player*)))))

   (draw-text "Inventory:"
              (tgk:add *window-top-right-corner*
                       (tgk:vec2 -495 -400))
              :fill-color highlight-color
              :font big-font)
   (draw-string-list (tgk:add *window-top-right-corner* (tgk:vec2 -495 -430))
                     30
                     regular-color
                     small-font
                     small-font-size
                     (list (format nil "Health potions: ~A" (gethash :health-potion (inventory *player*)))
                           (format nil "Mana potions: ~A"   (gethash :mana-potion   (inventory *player*)))
                           (format nil "Hellfire Key: ~A"   (gethash :hellfire-key   (inventory *player*)))))))


(defun draw-player-vision-radius (&optional (p *player*))
  (for-cells-in-view-of (*dungeon-camera* x y cell-pos)
    (gamekit:draw-rect (vector->vec2
                        (position-inside-camera
                         *dungeon-camera*
                         (cell-position->pixel/center
                          cell-pos)))
                       5
                       5
                       :fill-paint
                       (cond ((and (within-circle-p (vision-radius p)
                                                     (elt (seq- cell-pos (dungeon-pos p)) 0)
                                                     (elt (seq- cell-pos (dungeon-pos p)) 1))
                                   (tangential (dungeon-pos p)
                                               (vision-radius p)
                                               cell-pos))
                              +color-red+)
                             ((within-circle-p (vision-radius p)
                                               (elt (seq- cell-pos (dungeon-pos p)) 0)
                                               (elt (seq- cell-pos (dungeon-pos p)) 1))
                              +color-antiquewhite2+)
                             (t (alpha-color .7 +color-antiquewhite2+)))))
  (draw-vision-circle *player*))


(defun inside-drawing-queue-p (k)
  (dolist (d *drawing-queue*)
    (when (eq (first d) k)
      (return-from inside-drawing-queue-p t))))

(defun refresh-drawing-timer (keys)
  (dolist (k keys)
    (setf (cadr (assoc k *drawing-queue*)) (+ (now) 1.5))))


(defmethod render ((a animation) &optional (pos *window-bottom-left-corner*))
  (let* ((frame (get-frame a (now)))
         (origin (keyframe-origin frame))
         (flipped-x (keyframe-flipped-x frame))
         (flipped-y (keyframe-flipped-y frame)))
    (draw-sprite (keyframe-image frame)
                 (tgk:add origin pos))))
