(in-package "DMOMD")

(defvar *animation-dungeon-player-idle* nil)
(defvar *animation-dungeon-player-north* nil)
(defvar *animation-dungeon-player-south* nil)
(defvar *animation-dungeon-player-east* nil)
(defvar *animation-dungeon-player-west* nil)
(defvar *animation-battle-player-idle* nil)
(defvar *animation-battle-player-hurt* nil)
(defvar *animation-battle-player-dying* nil)
(defvar *animation-battle-player-attack-slash* nil)
(defvar *animation-battle-player-attack-slash-effect* nil)
(defvar *animation-battle-player-attack-fireball* nil)
(defvar *animation-battle-player-attack-fireball-effect* nil)
(defvar *animation-battle-player-attack-lightning* nil)
(defvar *animation-battle-player-attack-lightning-effect* nil)
(defvar *animation-battle-player-attack-deny-existence* nil)
(defvar *animation-battle-player-attack-deny-existence-effect* nil)
(defvar *animation-battle-player-item-health-potion* nil)
(defvar *animation-battle-player-item-mana-potion* nil)


(defclass player (animatable positionable can-fight)
  ((inventory
    :accessor inventory
    :initarg :inventory
    :initform (make-hash-table)))

  (:default-initargs
   :vision-radius 3.4
    :hp-max 70
    :mp-max 100
    :agility  2
    :strength 2
    :intelligence 5
    :initiative    2
    :physical-defense 1
    :vulnerabilities (list)
    :resistances (list)
    :battle-draw-pos (tgk:vec2 100 200)
    :attack-speed 0.7))

(defmethod initialize-instance :after ((p player) &key)
  (setf (vision-mask p) (make-mask (vision-radius p)))
  (setf (current-animation p)
        *animation-dungeon-player-idle*)
  (setf (dungeon-animations p)
        (list (cons :idle
                    *animation-dungeon-player-idle*)))
  (setf (battle-animations p)
        (list (cons :idle
                    *animation-battle-player-idle*)
              (cons :hurt
                    *animation-battle-player-hurt*)
              (cons :dying
                    *animation-battle-player-dying*)
              (cons :attack-slash
                    *animation-battle-player-attack-slash*)
              (cons :attack-fireball
                    *animation-battle-player-attack-fireball*)
              (cons :attack-lightning
                    *animation-battle-player-attack-lightning*)
              (cons :attack-deny-existence
                    *animation-battle-player-attack-deny-existence*)
              (cons :item-health-potion
                    *animation-battle-player-item-health-potion*)
              (cons :item-mana-potion
                    *animation-battle-player-item-mana-potion*)))
  (setf (battle-attack-effect-animations p)
        (list (cons :attack-slash
                    *animation-battle-player-attack-slash-effect*)
              (cons :attack-fireball
                    *animation-battle-player-attack-fireball-effect*)
              (cons :attack-lightning
                    *animation-battle-player-attack-lightning-effect*)
              (cons :attack-deny-existence
                    *animation-battle-player-attack-deny-existence-effect*)))
  (setf (attacks p)
        (list (cons :attack-slash          *attack-slash*)
              (cons :attack-fireball       *attack-fireball*)
              (cons :attack-lightning      *attack-lightning*)
              (cons :attack-deny-existence *attack-deny-existence*)))
  (setf (hp p) (hp-max p))
  (setf (mp p) (mp-max p))

  (setf (gethash :health-potion (inventory p)) 0)
  (setf (gethash :mana-potion (inventory p)) 0)
  (setf (gethash :hellfire-key (inventory p)) 0))

(defun player-not-attacking-p (&optional (p *player*))
  (not (attacking-p p)))

(defmethod player-attack-callbacks (&optional (p *player*))
  (mapcar (lambda (x) (attack-callback (cdr x))) (attacks p)))

(defmethod player-battle-animation-objects (&optional (p *player*))
  "return an attack object indexed by the key `a'."
  (let (result)
    (dolist (anim (battle-animations p))
      (push
       (cdr anim)
       result))
    result))

(defun player-pos (&optional (p *player*))
  (dungeon-pos p))

(defun player-pos-for-aref (&optional (player *player*))
  (values (elt (dungeon-pos player) 0)
          (elt (dungeon-pos player) 1)))

(defmethod update-vision-mask ((p player) radius)
  (setf (vision-radius p) radius)
  (setf (vision-mask   p) (make-mask radius))
  (update-view (player-pos)))

(defun spawn-player ()
  (if *last-safe-cell* ; if the player was already spawned once
      (teleport-to! *last-safe-cell*)
      ;; else
      (let ((new-player-pos (calc-spawn-pos)))
        (format t "~&new-player-pos: ~A~%" new-player-pos)
        (teleport-to! new-player-pos)
        (setf *last-safe-cell* new-player-pos))))

(defun calc-spawn-pos ()
  ;; *dungeon-empty-rooms* needs to have been populater prior
  (let* ((r (pop *dungeon-empty-rooms*))
         (random-pos (a:random-elt (gethash r *dungeon-rooms*))))
    random-pos
    ;; (if (valid-spawn-pos-p random-pos)
    ;;     (calc-spawn-pos))
    ))

(defun player-update-vision-mask (&optional (p *player*))
  "Updates and return the players vision-mask.
The vision area is a 2D array."
  nil)

(defun player-make-vision-mask-relative-to-vision-center (&optional (p *player*))
  (dotimes (x (array-dimension (vision-mask-array) 0))  ; the array is a square array
    (dotimes (y (array-dimension (vision-mask-array) 1))
      (setf (aref (vision-mask-array) x y)
            (seq- (aref (vision-mask-array) x y)
                  (vision-mask-center))))))

(defun vision-mask-center (&optional (p *player*))
  (vector (vision-radius p) (player-vision-radius p)))

(defun player-visible-cells (&optional (p *player*))
  "Returns a list of global visible cell coordinates."
  (relatives-to-absolutes (dungeon-pos  p)
                          (mask-to-relative (vision-mask p))))

(defun vision-mask-array (&optional (p *player*))
  (mask-array (vision-mask p)))

(defun player-in-enemy-view-p (enemy &optional (p *player*))
  (and (member (dungeon-pos p)
               (relatives-to-absolutes ;; absolute position of enemy mask
                (dungeon-pos enemy)
                (mask-to-relative (vision-mask enemy)))
               :test #'equalp)
       (eql (dungen::cell-region (aref *grid*
                                       (elt (dungeon-pos enemy) 0)
                                       (elt (dungeon-pos enemy) 1)))
            (dungen::cell-region (aref *grid*
                                       (elt (dungeon-pos p) 0)
                                       (elt (dungeon-pos p) 1))))))

;; (defun valid-spawn-pos-p (pos)
;;   (cond ((dungen::has-feature-p (aref *grid* (elt pos 0) (elt pos 1)) :wall)
;;          nil)
;;         (t t)))

(defun cell-positon-relative-to-player (cell &optional (player *player*))
  (seq+ (dungeon-pos player) cell))

(defmethod attack-enemies (atk-keyword enemy-list (p player))
  "Attacks each enemy inside `enemy-list'"
  (dolist (e enemy-list)
    (attack p e atk-keyword)))


(defmethod attack :before ((p player) (attacked can-fight) atk-keyword)
  (play-sound (attack-sound (get-attack atk-keyword p)))
  ;; start attack animation
  (setf (current-animation p)
        (get-animation atk-keyword (battle-animations p)))
  (start-animation (current-animation p) (now))
  ;; start attack effect animation
  (push (get-animation atk-keyword (battle-attack-effect-animations p)) *effect-animations*)
  (start-animation (get-animation atk-keyword (battle-attack-effect-animations p))
                   (now)
                   :position (battle-draw-pos attacked)))

(defmethod attack ((p player) (e slime) atk-keyword)
  "Player performs the attack defined by atk-keyword.
Sleep, paralysis, etc. was already taken into account.
Who attacks whom with what.
`p': who, `e': whom `atk-keyword': with what"
  (call-next-method p e atk-keyword))

(defmethod attack  :after ((p player) (s slime) atk-keyword))

;;;;-----------------------------------------------------------------------------
;;;; Drawing
;;;;-----------------------------------------------------------------------------

(defmethod draw ((p player) &optional (draw-pos (battle-draw-pos *player*)))
  (call-next-method p draw-pos))


(defmethod render-animation ((a animation) (p player) window-pos)
  (unless (null a)
    (let* ((frame (get-frame a (now)))
           (origin (keyframe-origin frame))
           (flipped-x (keyframe-flipped-x frame))
           (flipped-y (keyframe-flipped-y frame)))
      (tgk:with-pushed-canvas ()
        ;; draw appropriate frame/sprite
        (draw-sprite (keyframe-image frame)
                     (tgk:add origin window-pos)))
      ;; after the animation has finished, change the current-animation to be the default
      (when (animation-finished-p a (now))
        (cond
          ;; rogue mode
          ((eql (type-of *mode*) 'rogue-mode)
           (setf (current-animation p)
                 *animation-dungeon-player-idle*))
          ;; battle mode
          ((eql (type-of *mode*) 'battle-mode)
           (setf (attacking-p p) nil)
           (cond ((or (not (dead-p *player*))
                      *player-invincible*)
                  (setf (current-animation p) *animation-battle-player-idle*))
                 ((dead-p *player*)
                  ;; (setf (current-animation p) *animation-battle-player-dying*)
                  )
                 (t (error "player seems to be neither dead nor alive." ))))))
      ;; (if (and flipped-x flipped-y)
      ;;     (progn
      ;;       (setf position
      ;;             (tgk:vec2
      ;;              (tgk:x position)
      ;;              (tgk:y position)))
      ;;       (tgk:scale-canvas -1 -1))
      ;;     (if flipped-x
      ;;         (progn
      ;;           (setf position
      ;;                 (tgk:vec2
      ;;                  (tgk:x position)
      ;;                  (tgk:y position)))
      ;;           (tgk:scale-canvas -1 1))
      ;;         (if flipped-y
      ;;             (progn
      ;;               (setf position
      ;;                     (tgk:vec2
      ;;                      (tgk:x position)
      ;;                      (tgk:y position)))
      ;;               (tgk:scale-canvas 1 -1)))))
      )))

(defun create-animations-player ()
  ;; player dungeon animations
  (setf *animation-dungeon-player-idle*
        (make-animation "dungeon-player-idle"
                        (list
                         (list (get-resource '(:img :dungeon :player :south)) 0
                               (tgk:vec2 0 0) nil nil))
                        1 :looped-p t))

  (setf *animation-dungeon-player-north*
        (make-animation "dungeon-player-north"
                        (list
                         (list (get-resource '(:img :dungeon :player :north)) 0
                               (tgk:vec2 0 0) nil nil))
                        1 :looped-p t))

  (setf *animation-dungeon-player-south*
        (make-animation "dungeon-player-south"
                        (list
                         (list (get-resource '(:img :dungeon :player :south)) 0
                               (tgk:vec2 0 0) nil nil))
                        1 :looped-p t))

  (setf *animation-dungeon-player-east*
        (make-animation "dungeon-player-east"
                        (list
                         (list (get-resource '(:img :dungeon :player :east)) 0
                               (tgk:vec2 0 0) nil nil))
                        1 :looped-p t))

  (setf *animation-dungeon-player-west*
        (make-animation "dungeon-player-west"
                        (list
                         (list (get-resource '(:img :dungeon :player :west)) 0
                               (tgk:vec2 0 0) nil nil))
                        1 :looped-p t))

   ;; player battle animations


  (setf *animation-battle-player-idle*
        (make-animation "battle-player-idle"
                        (list
                         (list (get-resource '(:img :battle :player :idle)) 0
                               (tgk:vec2 0 0) nil nil))
                        1 :looped-p t))

  (setf *animation-battle-player-hurt*
        (make-animation "battle-player-hurt"
                        (list
                         (list (get-resource '(:img :battle :player :hurt)) 0
                               (tgk:vec2 0 0) nil nil))
                        .4 :looped-p nil))


  (setf *animation-battle-player-dying*
        (make-animation "battle-player-dying"
                        (list
                         (list (get-resource '(:img :battle :player :dying)) 0
                               (tgk:vec2 0 -40) nil nil))
                        .4 :looped-p nil))


   ;; player attack animations

  (setf *animation-battle-player-attack-slash*
        (make-animation "battle-player-attack-slash"
                        (list
                         (list (get-resource '(:img :battle :player :attack-slash)) 0
                               (tgk:vec2 400 0) nil nil))
                        .5 :looped-p nil))

  (setf *animation-battle-player-attack-slash-effect*
        (make-animation "battle-player-attack-slash-effect"
                        (list
                         (list (get-resource '(:img :battle :player :attack-slash-effect)) 0
                               (tgk:vec2 0 0) nil nil))
                        .35
                        :looped-p nil
                        :position *battle-enemy-default-pos*))


  (setf *animation-battle-player-attack-fireball*
        (make-animation "battle-player-attack-fireball"
                        (list
                         (list (get-resource '(:img :battle :player :attack-fireball)) 0
                               (tgk:vec2 0 0) nil nil))
                        .5 :looped-p nil))
  (setf *animation-battle-player-attack-fireball-effect*
        (make-animation "battle-player-attack-fireball-effect"
                        (list
                         (list (get-resource '(:img :battle :player :attack-fireball-effect)) 0
                               (tgk:vec2 0 0) nil nil))
                        .35
                        :looped-p nil
                        :position *battle-enemy-default-pos*))


  (setf *animation-battle-player-attack-lightning*
        (make-animation "battle-player-attack-lightning"
                        (list
                         (list (get-resource '(:img :battle :player :attack-lightning)) 0
                               (tgk:vec2 0 -10) nil nil))
                        .5 :looped-p nil))
  (setf *animation-battle-player-attack-lightning-effect*
        (make-animation "battle-player-attack-lightning-effect"
                        (list
                         (list (get-resource '(:img :battle :player :attack-lightning-effect)) 0
                               (tgk:vec2 0 0) nil nil))
                        .35
                        :looped-p nil
                        :position *battle-enemy-default-pos*))
  (setf *animation-battle-player-attack-deny-existence*
        (make-animation "battle-player-attack-deny-existence"
                        (list
                         (list (get-resource '(:img :battle :player :attack-deny-existence1)) 0
                               (tgk:vec2 0 0) nil nil)
                         (list (get-resource '(:img :battle :player :attack-deny-existence2)) .25
                               (tgk:vec2 0 0) nil nil))
                        .5 :looped-p nil))
  (setf *animation-battle-player-attack-deny-existence-effect*
        (make-animation "battle-player-attack-deny-existence-effect"
                        (list
                         (list (get-resource '(:img :battle :player :attack-deny-existence-effect1)) 0
                               (tgk:vec2 0 0) nil nil)
                         (list (get-resource '(:img :battle :player :attack-deny-existence-effect2)) .25
                               (tgk:vec2 0 0) nil nil))
                        .5
                        :looped-p nil
                        :position *battle-enemy-default-pos*))

  (setf *animation-battle-player-item-health-potion*
        (make-animation "battle-player-item-health-potion"
                        (list
                         (list (get-resource '(:img :battle :player :item-health-potion-1)) 0
                               (tgk:vec2 +5 -15) nil nil)
                         (list (get-resource '(:img :battle :player :item-health-potion-2)) .25
                               (tgk:vec2 +5 -13) nil nil))
                        .5
                        :looped-p nil))
  (setf *animation-battle-player-item-mana-potion*
        (make-animation "battle-player-item-mana-potion"
                        (list
                         (list (get-resource '(:img :battle :player :item-mana-potion-1)) 0
                               (tgk:vec2 +5 -15) nil nil)
                         (list (get-resource '(:img :battle :player :item-mana-potion-2)) .25
                               (tgk:vec2 +5 -15) nil nil))
                        .5
                        :looped-p nil))
  (setf *animation-credits-player-won*
        (make-animation "animation-credits-player-won"
                        (list
                         (list (get-resource '(:img :credits :player :won-0)) 0
                               (tgk:vec2 0 0) nil nil)
                         (list (get-resource '(:img :credits :player :won-1)) 1
                               (tgk:vec2 0 0) nil nil))
                        *dungeon-leave-animation-duration*
                        :looped-p nil)))


(defmethod battle-try-run ((p player) enemies)
  (if (can-run-away-p p enemies)
      (battle-run)))

(defmethod can-run-away-p ((p player) enemies)
  "t means you ran away successfully, nil means you failed."
  (if *in-boss-battle-p*
      (progn
        (draw-for (.8 :cant-run-notification)
          `(draw-text ,(princ-to-string "I can't run away!")
                      ,(tgk:add *window-center* (tgk:vec2 -320 +200))
                      :fill-color ,+color-red1+
                      :font ,(font :quikhand 100)))
       nil)
      (if (chance-% 50)
          (progn
            (draw-for (.85 :cant-run-notification)
              `(draw-text ,(princ-to-string "Damn it! I wasn't quick enough.")
                          ,(tgk:add *window-center* (tgk:vec2 -320 +200))
                          :fill-color ,+color-red1+
                          :font ,(font :quikhand 40)))
            nil)
          t)))


(defmethod level-up ((p player)  &key (how-many-lvls 1))
  (with-accessors ((agi agility)
                   (str strength)
                   (int intelligence)
                   (pd physical-defense)
                   (ini initiative)
                   (hp hp)
                   (hp-max hp-max)
                   (mp mp)
                   (mp-max mp-max)
                   (level level))
      p
    (dotimes (i how-many-lvls)
      (incf level)
      (incf hp 10)
      (incf hp-max 10)
      (incf mp 4)
      (incf mp-max 4)

      (incf agi 1)
      (incf ini 1)
      (incf pd  1)
      (if (chance-% 50)
          (incf str 2)
          (incf str 1))
      (if (chance-% 30)
          (incf int 2)
          (incf int 1)))
    ;; (add-timer (+ (now) 1)
    ;;            (lambda ()))
    (play-sound :snd-level-up)
    (draw-for (.5 :level-up-notification)
      `(draw-text ,(format nil "LEVEL ~A REACHED." level)
                  ,(tgk:add *window-center* (tgk:vec2 -200 0))
                  :fill-color ,+color-gold3+
                  :font ,(font :consola 80)))))

(defmethod has-item (item-keyword (p player))
  (> (gethash item-keyword (inventory *player*)) 0))

(defmethod has-item-p (item (p player))
  (> (gethash item (inventory p)) 0))

(defun player-can-kill-enemy-p ()
    (not *player-can-not-kill-enemy*))

(defmethod player-animation-is-p (anim-key &optional (p *player*))
  (equalp (current-animation p)
          (get-animation anim-key (battle-animations p))))

(defun give-player-item (item-keyword &optional (amount 1))
  (incf (gethash item-keyword (inventory *player*)) amount))

(defun take-player-item (item-keyword &optional (amount 1))
  (decf (gethash item-keyword (inventory *player*))
        (min amount (gethash item-keyword (inventory *player*)))))
