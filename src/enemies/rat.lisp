(in-package #:dmomd)

(defvar *animation-dungeon-rat-north* nil)
(defvar *animation-dungeon-rat-south* nil)
(defvar *animation-dungeon-rat-east* nil)
(defvar *animation-dungeon-rat-west* nil)
(defvar *animation-battle-rat-idle* nil)
(defvar *animation-battle-rat-hurt* nil)
(defvar *animation-battle-rat-dying* nil)
(defvar *animation-battle-rat-attack-bite* nil)


(defclass rat (enemy)
  ((vision-mask
    :initform (make-mask (enemy-vision-radius 'rat))))
  (:default-initargs
   :hp       30
    :hp-max  30

    :xp 0
    :xp-total 100


    :agility       3
    :strength      2
    :intelligence  2
    :initiative    3

    :physical-defense  1
    :vulnerabilities  (list :fire :lightning)
    :resistances      (list :poison :water)))

(defmethod print-object ((r rat) out)
  (with-slots (hp hp-max dungeon-pos)
      r
    (print-unreadable-object (r out)
      (format out "Rat: ~A/~A at ~A" hp hp-max dungeon-pos))))

(defmethod initialize-instance :after ((r rat) &key)
  (setf (current-animation r)
        *animation-dungeon-rat-south*)
  (setf (dungeon-animations r)
        (list (cons :idle *animation-dungeon-rat-south*)
              (cons :north *animation-dungeon-rat-north*)
              (cons :south *animation-dungeon-rat-south*)
              (cons :east  *animation-dungeon-rat-east*)
              (cons :west  *animation-dungeon-rat-west*)))
  (setf (battle-animations r)
        (list (cons :idle  *animation-battle-rat-idle*)
              (cons :hurt  *animation-battle-rat-hurt*)
              (cons :dying *animation-battle-rat-dying*)
              (cons :attack-bite  *animation-battle-rat-attack-bite*)))
  (setf (attacks r)
        (list (cons :attack-bite *attack-bite*))))

(defmethod attack :before ((r rat) (attacked can-fight) atk-keyword)
  (play-sound (attack-sound (get-attack atk-keyword r)))
  ;; start attack animation
  (setf (current-animation r)
        (get-animation atk-keyword (battle-animations r)))
  (start-animation (current-animation r) (now))
  ;; start attack effect animation
  ;; (push (get-animation atk-keyword (battle-attack-effect-animations r)) *effect-animations*)
  ;; (start-animation (get-animation atk-keyword (battle-attack-effect-animations r)) (now))
  )

(defmethod draw ((e rat) &optional (draw-pos (battle-draw-pos e)))
  (call-next-method e draw-pos))

(defmethod render-animation ((a animation) (r rat) window-pos)
  (unless (null a)
    (let* ((frame (get-frame a (now)))
           (origin (keyframe-origin frame))
           (flipped-x (keyframe-flipped-x frame))
           (flipped-y (keyframe-flipped-y frame))
           (position (position-of a)))
      (tgk:with-pushed-canvas ()
        (when (eq (type-of *mode*) 'battle-mode)
          (tgk:translate-canvas 160 40)
          (gamekit:scale-canvas .7 .7))
        (draw-sprite (keyframe-image frame)
                     (tgk:add origin window-pos)))

      ;; after the animation has finished, change the current-animation to be the default
      (when (animation-finished-p a (now))
        (cond ((eql (type-of *mode*) 'rogue-mode)
               (setf (current-animation r)
                     (get-animation :idle (dungeon-animations r))))

              ((eql (type-of *mode*) 'battle-mode)
               (setf (attacking-p r) nil)
               (unless (dead-p r)
                 (setf (current-animation r)
                       (get-animation :idle (battle-animations r)))))))
      ;; (if (and flipped-x flipped-y)
      ;;     (progn
      ;;       (setf position
      ;;             (tgk:vec2
      ;;              (tgk:x position)
      ;;              (tgk:y position)))
      ;;       (tgk:scale-canvas -1 -1))
      ;;     (if flipped-x
      ;;         (progn
      ;;           (setf position
      ;;                 (tgk:vec2
      ;;                  (tgk:x position)
      ;;                  (tgk:y position)))
      ;;           (tgk:scale-canvas -1 1))
      ;;         (if flipped-y
      ;;             (progn
      ;;               (setf position
      ;;                     (tgk:vec2
      ;;                      (tgk:x position)
      ;;                      (tgk:y position)))
      ;;               (tgk:scale-canvas 1 -1)))))
      )))

;;;;-----------------------------------------------------------------------------
;;;; Define Animations
;;;;-----------------------------------------------------------------------------

(defun create-animations-rat ()
  ;; dungeon animations

  (setf *animation-dungeon-rat-north*
        (make-animation "dungeon-rat-north"
                        (list
                         (list (get-resource '(:img :dungeon :rat :north)) 0
                               (tgk:vec2 0 0) nil nil))
                        .5 :looped-p t))

  (setf *animation-dungeon-rat-south*
        (make-animation "dungeon-rat-south"
                        (list
                         (list (get-resource '(:img :dungeon :rat :south)) 0
                               (tgk:vec2 0 0) nil nil))
                        .5 :looped-p t))

  (setf *animation-dungeon-rat-east*
        (make-animation "dungeon-rat-east"
                        (list
                         (list (get-resource '(:img :dungeon :rat :east)) 0
                               (tgk:vec2 0 0) nil nil))
                        .5 :looped-p t))

  (setf *animation-dungeon-rat-west*
        (make-animation "dungeon-rat-west"
                        (list
                         (list (get-resource '(:img :dungeon :rat :west)) 0
                               (tgk:vec2 0 0) nil nil))
                        .5 :looped-p t))

   ;; battle animations
  (setf *animation-battle-rat-idle*
        (make-animation "battle-rat-idle"
                        (list
                         (list (get-resource '(:img :battle :rat :idle)) 0
                               (tgk:vec2 0 0) nil nil))
                        .5 :looped-p nil))

  (setf *animation-battle-rat-hurt*
        (make-animation "battle-rat-hurt"
                        (list
                         (list (get-resource '(:img :battle :rat :hurt)) 0
                               (tgk:vec2 70 20) nil nil))
                        .5 :looped-p nil))

  (setf *animation-battle-rat-dying*
        (make-animation "battle-rat-dying"
                        (list
                         (list (get-resource '(:img :battle :rat :dying)) 0
                               (tgk:vec2 0 -5) nil nil))
                         *enemy-death-anim-duration* :looped-p nil))

  (setf *animation-battle-rat-attack-bite*
        (make-animation "battle-rat-attack-bite"
                        (list
                         (list (get-resource '(:img :battle :rat :attack-bite)) 0
                               (tgk:vec2 -730 -20) nil nil))
                        .5 :looped-p nil)))

(defmethod draw-aggression-mask ((r rat) &optional (opacity .5))
  (dolist (aggression-pos (relatives-to-absolutes (dungeon-pos r)
                                                  (mask-to-relative (vision-mask r))))
    ;; loop over all coordinates that could potentially be drawn
    (when (valid-aggression-tile-pos-p aggression-pos r)  ; check if it should be drawn
      (draw-cell aggression-pos (tgk:vec4 .3 .2 .15 opacity)))))
