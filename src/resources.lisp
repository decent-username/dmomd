(in-package #:dmomd)


;;; https://gist.github.com/borodust/b75f964aa405c9d1ffa4be4654e3cbb1
(defun prepare-resources ()
  ;; prepare the resources
  (loop
     :for r :in (mapcar #'cdr (sort (copy-list *resources*) #'< :key #'car))
     :for i :from 1
     :do (let ((r r))
           (bodge-concurrency:in-new-thread ("resource-loading")
             (when (tgk:gamekit)
               (tgk:push-action (lambda () (tgk:prepare-resources r)
                                  (push r *loading-resources*)
                                  (incf *number-of-registered-resources*))))))))


(defun img-lookup (sprite suffix)
  (intern (concatenate 'string "IMG-" (symbol-name sprite) "-" (symbol-name suffix)) "KEYWORD"))

;;; wrapping trivial-gamekit drawing functions ---------------------------------
(defmacro define-font (name path &key (priority 0)) ; sort them based on prio and load in that order
  `(progn
     (tgk:define-font ,name ,path)
     (push (cons ,priority ,name) *resources*)))

(defmacro define-sound (name path &key (priority 0))
  `(progn
     (tgk:define-sound ,name ,path)
     (push (cons ,priority ,name) *resources*)))

(defmacro define-image (name path &key (priority 0))
  `(progn
     (tgk:define-image ,name ,path)
     (push (cons ,priority ,name) *resources*)))

(defun resource-loaded-p (keyword)
  (member keyword *loaded-resources*))

;;;;-----------------------------------------------------------------------------
;;;; Defining Global Resources
;;;;-----------------------------------------------------------------------------
(tgk:register-resource-package
 :keyword (asdf:system-relative-pathname :dmomd "assets/"))

;;;; Fonts
(define-font :quikhand "fnt/quikhand.ttf" :priority 0)
(define-font :consola "fnt/ConsolaMono-Bold.ttf" :priority 5)

;;;; Sounds
;;; Music
(define-sound :snd-title-mode-music  "snd/title-mode/title-mode-music.ogg"   :priority  1)
(define-sound :snd-rogue-mode-music  "snd/rogue-mode/rogue-mode-music.wav"   :priority 10)
(define-sound :snd-battle-mode-music "snd/battle-mode/battle-mode-music.wav" :priority 10)
(define-sound :snd-boss-battle-music "snd/battle-mode/boss-battle-music.ogg" :priority 10)

;;; UI
(define-sound :snd-button-change "snd/interface/switch9.ogg"  :priority 10)
(define-sound :snd-button-click  "snd/interface/switch29.ogg" :priority 10)
(define-sound :snd-gasp          "snd/misc/gasp1.ogg" :priority 10)

;;; Items
(define-sound :snd-mana-potion   "snd/inventory/bubble.wav"  :priority 10)
(define-sound :snd-health-potion "snd/inventory/bubble2.wav" :priority 10)
(define-sound :snd-chest         "snd/inventory/coin.wav"    :priority 10)
(define-sound :snd-hellfire-key  "snd/misc/Tech-26.wav"    :priority 10)


;;; Other SFX
(define-sound :snd-door-open "snd/rogue-mode/door-open.ogg" :priority 10)

(define-sound :snd-hellfire-door-locked "snd/rogue-mode/hellfire-door-locked.ogg" :priority 10)
(define-sound :snd-hellfire-door-open   "snd/rogue-mode/hellfire-door-open.wav"   :priority 10)

(define-sound :snd-walk-1 "snd/rogue-mode/walk/walk-1.ogg" :priority 10)
(define-sound :snd-walk-2 "snd/rogue-mode/walk/walk-2.ogg" :priority 10)
(define-sound :snd-walk-3 "snd/rogue-mode/walk/walk-3.ogg" :priority 10)
(define-sound :snd-walk-4 "snd/rogue-mode/walk/walk-4.ogg" :priority 10)

;;; Demon sounds effects
(define-sound :snd-demon-laugh "snd/rogue-mode/evil-sound.wav" :priority 10)

(define-sound :snd-demon-attack-1 "snd/battle-mode/demon/demon-attack-1.wav" :priority 10)
(define-sound :snd-demon-attack-3 "snd/battle-mode/demon/demon-attack-3.wav" :priority 10)
(define-sound :snd-demon-death "snd/battle-mode/demon/demon-death.wav" :priority 10)
(define-sound :snd-demon-hurt-1 "snd/battle-mode/demon/demon-hurt.wav" :priority 10)
(define-sound :snd-demon-hurt-2 "snd/battle-mode/demon/demon-hurt-much.wav" :priority 10)



;;; Attacks
(define-sound :snd-attack-slash     "snd/battle-mode/sword-unsheathe.wav"    :priority 10)
(define-sound :snd-attack-demon-slash     "snd/battle-mode/demon-slash.wav"    :priority 10)

(define-sound :snd-attack-fireball      "snd/fire/fire-attack.ogg"           :priority 10)
(define-sound :snd-attack-fireball-hit-1 "snd/battle-mode/spell_fire_06.ogg" :priority 10)
(define-sound :snd-attack-fireball-hit  "snd/fire/fire-hit.ogg"              :priority 10)

(define-sound :snd-attack-lightning "snd/electricity/electricity-1.ogg" :priority 10)

(define-sound :snd-attack-deny-existence "snd/battle-mode/deny-existence.wav" :priority 10)

(define-sound :snd-attack-slimeball     "snd/battle-mode/slime/slime10.wav" :priority 10)
(define-sound :snd-attack-slimeball-hit "snd/battle-mode/slime/slime9.wav"  :priority 10)

(define-sound :snd-attack-bite "snd/battle-mode/rat/rat-bite.wav"  :priority 10)

;;; Miscellanous
(define-sound :snd-level-up "snd/misc/jingle-level-up.wav"  :priority 10)
(define-sound :snd-lose "snd/misc/jingle-lose.wav"  :priority 10)
(define-sound :snd-win  "snd/misc/jingle-win.wav"  :priority 10)

(define-sound :snd-item-pickup "snd/rogue-mode/item-pickup.wav"  :priority 10)

;;;; Images
;;; Splash screen
(define-image :img-splash "img/title-mode/splash-alt-1280x720.png" :priority 1)

;;; Backgrounds
(define-image :img-battle-background-1 "img/battle-mode/battle-background-1-1280x720.png" :priority 10)

;; Credits Mode
(define-image :img-credits-background-1 "img/credits-mode/win-credits-1.png" :priority 10)
(define-image :img-credits-background-2-fg "img/credits-mode/win-credits-2-fg.png" :priority 10)
(define-image :img-credits-background-2-bg "img/credits-mode/win-credits-2-bg.png" :priority 10)

(define-image :img-credits-player-won-0 "img/credits-mode/player-won-0.png" :priority 10)
(add-resource '(:img :credits :player :won-0)
              (make-sprite :image :img-credits-player-won-0))
(define-image :img-credits-player-won-1 "img/credits-mode/player-won-1.png" :priority 10)
(add-resource '(:img :credits :player :won-1)
              (make-sprite :image :img-credits-player-won-1))

;;; UI
;; (define-image :img-ui-lens-flare "img/miscellaneous/ui-lens-flare.png"   :priority 1)
(define-image :img-ui-life-bar "img/miscellaneous/life-bar-w:200.png"    :priority 10)
(define-image :img-ui-enemy-hp-bar-overlay "img/battle-mode/enemy-health-bar-overlay.png" :priority 10)
(define-image :img-ui-demon-hp-bar-overlay "img/battle-mode/demon-health-bar-overlay.png" :priority 10)

;;; Boss battle enter
(define-image :img-first-person-chest "img/rogue-mode/first-person-chest.png" :priority 10)
(define-image :img-first-person-hands "img/rogue-mode/first-person-hands.png" :priority 10)

;;;; Sprite Sheets
;;;; --------------------
(define-image :img-tile-map
    "img/rogue-mode/tile-map.png" :priority 3)
(define-image :img-top-down-sprite-sheet
    "img/rogue-mode/top-down-sprite-sheet-512x512.png" :priority 3)
;;; Player
(define-image :img-battle-player-sprite-sheet
    "img/battle-mode/player/player-sprite-sheet-1080x1294.png" :priority 5)
;;; Slime
(define-image :img-battle-slime-sprite-sheet
    "img/battle-mode/slime/slime-sprite-sheet-648x432.png" :priority 6)
;;; Rat
(define-image :img-battle-rat-sprite-sheet
    "img/battle-mode/rat/rat-sprite-sheet-1080x720.png" :priority 6)
;;; ;; Zombie
;;; (define-image :img-battle-slime-sprite-sheet
;;;     "img/battle-mode/slime/slime-sprite-sheet-648x432.png" :priority 6)
;;; Demon
(define-image :img-battle-demon-sprite-sheet
    "img/battle-mode/demon/demon-sprite-sheet.png" :priority 6)

;;;;-----------------------------------------------------------------------------
;;;; Tile map
;;;;-----------------------------------------------------------------------------
(add-resource '(:img :dungeon :floor-tile)
              (make-ss-sprite :ss :img-tile-map
                              :ts *top-down-ss-tile-size*
                              :origin '(0 0) :w 1 :h 1))

(add-resource '(:img :dungeon :wall-tile)
              (make-ss-sprite :ss :img-tile-map
                              :ts *top-down-ss-tile-size*
                              :origin '(0 1) :w 1 :h 1))

(add-resource '(:img :dungeon :door-h-tile)
              (make-ss-sprite :ss :img-tile-map
                              :ts *top-down-ss-tile-size*
                              :origin '(1 0) :w 1 :h 1))

(add-resource '(:img :dungeon :door-v-tile)
              (make-ss-sprite :ss :img-tile-map
                              :ts *top-down-ss-tile-size*
                              :origin '(1 1) :w 1 :h 1))

(add-resource '(:img :dungeon :hellfire-door-h-tile)
              (make-ss-sprite :ss :img-tile-map
                              :ts *top-down-ss-tile-size*
                              :origin '(2 0) :w 1 :h 1))

(add-resource '(:img :dungeon :hellfire-door-v-tile)
              (make-ss-sprite :ss :img-tile-map
                              :ts *top-down-ss-tile-size*
                              :origin '(2 1) :w 1 :h 1))

;;;;------------------------------------------------------------------------------
;;;; Items
;;;;------------------------------------------------------------------------------
(add-resource '(:img :dungeon :chest-closed)
              (make-ss-sprite :ss :img-top-down-sprite-sheet
                              :ts *top-down-ss-tile-size*
                              :origin '(0 4) :w 1 :h 1))

(add-resource '(:img :dungeon :chest-opened-empty)
              (make-ss-sprite :ss :img-top-down-sprite-sheet
                              :ts *top-down-ss-tile-size*
                              :origin '(0 5) :w 1 :h 1))

(add-resource '(:img :dungeon :chest-opened-full)
              (make-ss-sprite :ss :img-top-down-sprite-sheet
                              :ts *top-down-ss-tile-size*
                              :origin '(0 6) :w 1 :h 1))

(add-resource '(:img :dungeon :potion-health)
              (make-ss-sprite :ss :img-top-down-sprite-sheet
                              :ts *top-down-ss-tile-size*
                              :origin '(2 4) :w 1 :h 1))

(add-resource '(:img :dungeon :potion-mana)
              (make-ss-sprite :ss :img-top-down-sprite-sheet
                              :ts *top-down-ss-tile-size*
                              :origin '(2 5) :w 1 :h 1))

(add-resource '(:img :dungeon :hellfire-key)
              (make-ss-sprite :ss :img-top-down-sprite-sheet
                              :ts *top-down-ss-tile-size*
                              :origin '(3 4) :w 1 :h 1))



;;;;------------------------------------------------------------------------------
;;;; Player Dungeon Sprites
;;;;------------------------------------------------------------------------------
(add-resource '(:img :dungeon :player :south)
              (make-ss-sprite :ss :img-top-down-sprite-sheet
                              :ts *top-down-ss-tile-size*
                              :origin '(0 0) :w 1 :h 1))

(add-resource '(:img :dungeon :player :north)
              (make-ss-sprite :ss :img-top-down-sprite-sheet
                              :ts *top-down-ss-tile-size*
                              :origin '(0 1) :w 1 :h 1))

(add-resource '(:img :dungeon :player :east)
              (make-ss-sprite :ss :img-top-down-sprite-sheet
                              :ts *top-down-ss-tile-size*
                              :origin '(0 3) :w 1 :h 1))

(add-resource '(:img :dungeon :player :west)
              (make-ss-sprite :ss :img-top-down-sprite-sheet
                              :ts *top-down-ss-tile-size*
                              :origin '(0 2) :w 1 :h 1))

;;;;-----------------------------------------------------------------------------
;;;; Player Battle Sprites
;;;;-----------------------------------------------------------------------------

;;; slash ----------------------------------------------------------------------
(add-resource '(:img :battle :player :attack-slash)
              (make-ss-sprite :ss :img-battle-player-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(11 4) :w 4 :h 3))

(add-resource '(:img :battle :player :attack-slash-effect)
              (make-ss-sprite :ss :img-battle-player-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(13 1) :w 2 :h 3))

;;; fire ------------------------------------------------------------------------
(add-resource '(:img :battle :player :attack-fireball)
  (make-ss-sprite :ss :img-battle-player-sprite-sheet
                  :ts *battle-ss-tile-size*
                  :origin '(0 6) :w 6 :h 4))

(add-resource '(:img :battle :player :attack-fireball-effect)
  (make-ss-sprite :ss :img-battle-player-sprite-sheet
                  :ts *battle-ss-tile-size*
                  :origin '(6 6) :w 4 :h 4))

;;; lightning -------------------------------------------------------------------
(add-resource '(:img :battle :player :attack-lightning)
  (make-ss-sprite :ss :img-battle-player-sprite-sheet
                  :ts *battle-ss-tile-size*
                  :origin '(0 2) :w 6 :h 4))

(add-resource '(:img :battle :player :attack-lightning-effect)
  (make-ss-sprite :ss :img-battle-player-sprite-sheet
                  :ts *battle-ss-tile-size*
                  :origin '(6 2) :w 4 :h 4))


;;; deny-existence -------------------------------------------------------------------
(add-resource '(:img :battle :player :attack-deny-existence1)
              (make-ss-sprite :ss :img-battle-player-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(0 10) :w 3 :h 4))

(add-resource '(:img :battle :player :attack-deny-existence2)
              (make-ss-sprite :ss :img-battle-player-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(0 14) :w 3 :h 4))

(add-resource '(:img :battle :player :attack-deny-existence-effect1)
              (make-ss-sprite :ss :img-battle-player-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(3 10) :w 4 :h 4))

(add-resource '(:img :battle :player :attack-deny-existence-effect2)
              (make-ss-sprite :ss :img-battle-player-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(3 14) :w 4 :h 4))

;;; items ----------------------------------------------------------------------
(add-resource '(:img :battle :player :item-health-potion-1)
              (make-ss-sprite :ss :img-battle-player-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(7 10) :w 3 :h 4))

(add-resource '(:img :battle :player :item-health-potion-2)
              (make-ss-sprite :ss :img-battle-player-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(7 14) :w 3 :h 4))

(add-resource '(:img :battle :player :item-mana-potion-1)
              (make-ss-sprite :ss :img-battle-player-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(10 10) :w 3 :h 4))

(add-resource '(:img :battle :player :item-mana-potion-2)
              (make-ss-sprite :ss :img-battle-player-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(10 14) :w 3 :h 4))





;;; other ----------------------------------------------------------------------
(add-resource '(:img :battle :player :idle)
  (make-ss-sprite :ss :img-battle-player-sprite-sheet
                  :ts *battle-ss-tile-size*
                  :origin '(10 1) :w 3 :h 3))

(add-resource '(:img :battle :player :hurt)
  (make-ss-sprite :ss :img-battle-player-sprite-sheet
                  :ts *battle-ss-tile-size*
                  :origin '(11 7) :w 4 :h 3))

(add-resource '(:img :battle :player :dying)
  (make-ss-sprite :ss :img-battle-player-sprite-sheet
                  :ts *battle-ss-tile-size*
                  :origin '(0 0) :w 4 :h 2))


;;;;------------------------------------------------------------------------------
;;;; Slime Dungeon Sprites
;;;;------------------------------------------------------------------------------
(add-resource '(:img :dungeon :slime :south)
              (make-ss-sprite :ss :img-top-down-sprite-sheet
                              :ts *top-down-ss-tile-size*
                              :origin '(1 0) :w 1 :h 1))

(add-resource '(:img :dungeon :slime :north)
              (make-ss-sprite :ss :img-top-down-sprite-sheet
                              :ts *top-down-ss-tile-size*
                              :origin '(1 1) :w 1 :h 1))

(add-resource '(:img :dungeon :slime :east)
              (make-ss-sprite :ss :img-top-down-sprite-sheet
                              :ts *top-down-ss-tile-size*
                              :origin '(1 2) :w 1 :h 1))

(add-resource '(:img :dungeon :slime :west)
              (make-ss-sprite :ss :img-top-down-sprite-sheet
                              :ts *top-down-ss-tile-size*
                              :origin '(1 3) :w 1 :h 1))


;;;;-----------------------------------------------------------------------------
;;;; Slime Battle Sprites
;;;;-----------------------------------------------------------------------------

;;; Idle ------------------------------------------------------------
(add-resource '(:img :battle :slime :idle)
              (make-ss-sprite :ss :img-battle-slime-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(0 3) :w 3 :h 3))

;;; Slimeball ------------------------------------------------------------
(add-resource '(:img :battle :slime :attack-slimeball)
              (make-ss-sprite :ss :img-battle-slime-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(3 3) :w 3 :h 3))

(add-resource '(:img :battle :slime :attack-slimeball-effect)
              (make-ss-sprite :ss :img-battle-slime-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(6 0) :w 3 :h 3))

;;; other ------------------------------------------------------------
(add-resource '(:img :battle :slime :hurt)
              (make-ss-sprite :ss :img-battle-slime-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(6 3) :w 3 :h 3))

(add-resource '(:img :battle :slime :dying)
              (make-ss-sprite :ss :img-battle-slime-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(0 0) :w 6 :h 2))


;;;;-----------------------------------------------------------------------------
;;;; Rat Dungeon Sprites
;;;;-----------------------------------------------------------------------------

;;; Idle ------------------------------------------------------------
(add-resource '(:img :dungeon :rat :south)
              (make-ss-sprite :ss :img-top-down-sprite-sheet
                              :ts *top-down-ss-tile-size*
                              :origin '(3 0) :w 1 :h 1))

(add-resource '(:img :dungeon :rat :north)
              (make-ss-sprite :ss :img-top-down-sprite-sheet
                              :ts *top-down-ss-tile-size*
                              :origin '(3 1) :w 1 :h 1))

(add-resource '(:img :dungeon :rat :east)
              (make-ss-sprite :ss :img-top-down-sprite-sheet
                              :ts *top-down-ss-tile-size*
                              :origin '(3 2) :w 1 :h 1))

(add-resource '(:img :dungeon :rat :west)
              (make-ss-sprite :ss :img-top-down-sprite-sheet
                              :ts *top-down-ss-tile-size*
                              :origin '(3 3) :w 1 :h 1))


;;;;-----------------------------------------------------------------------------
;;;; Rat Battle Sprites
;;;;-----------------------------------------------------------------------------

;;; Idle ------------------------------------------------------------
(add-resource '(:img :battle :rat :idle)
              (make-ss-sprite :ss :img-battle-rat-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(0 0) :w 6 :h 5))
;;; attack ------------------------------------------------------------
(add-resource '(:img :battle :rat :attack-bite)
              (make-ss-sprite :ss :img-battle-rat-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(6 0) :w 7 :h 5))
;;; hurt ------------------------------------------------------------
(add-resource '(:img :battle :rat :hurt)
              (make-ss-sprite :ss :img-battle-rat-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(0 5) :w 5 :h 5))

;;; dying ------------------------------------------------------------
(add-resource '(:img :battle :rat :dying)
              (make-ss-sprite :ss :img-battle-rat-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(6 5) :w 7 :h 5))


;; ;;;-----------------------------------------------------------------------------
;; ;;; Zombie Dungeon Sprites
;; ;;;-----------------------------------------------------------------------------
;; ;; Idle ------------------------------------------------------------
;; (add-resource '(:img :dungeon :zombie :idle)
;;               (make-ss-sprite :ss :img-battle-zombie-sprite-sheet
;;                               :ts *top-down-tile-size*
;;                               :origin '(0 3) :w 3 :h 3))

;; ;;;-----------------------------------------------------------------------------
;; ;;; Zombie Battle Sprites
;; ;;;-----------------------------------------------------------------------------
;; ;; Idle ------------------------------------------------------------
;; (add-resource '(:img :battle :zombie :idle)
;;               (make-ss-sprite :ss :img-battle-zombie-sprite-sheet
;;                               :ts *top-down-tile-size*
;;                               :origin '(0 3) :w 3 :h 3))

;; ;;;-----------------------------------------------------------------------------
;; ;;; Demon Dungeon Sprites
;; ;;;-----------------------------------------------------------------------------
;; ;; Idle ------------------------------------------------------------
;; (add-resource '(:img :dungeon :demon :idle)
;;               (make-ss-sprite :ss :img-battle-demon-sprite-sheet
;;                               :ts *top-down-tile-size*
;;                               :origin '(0 3) :w 3 :h 3))

;;;-----------------------------------------------------------------------------
;;; Demon Battle Sprites
;;;-----------------------------------------------------------------------------
;; Idle ------------------------------------------------------------
(add-resource '(:img :battle :demon :idle)
              (make-ss-sprite :ss :img-battle-demon-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(15 4) :w 8 :h 8))


(add-resource '(:img :battle :demon :hurt)
              (make-ss-sprite :ss :img-battle-demon-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(14 13) :w 9 :h 8))


(add-resource '(:img :battle :demon :attack-slash)
              (make-ss-sprite :ss :img-battle-demon-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(9 22) :w 13 :h 8))



(add-resource '(:img :battle :demon :dying-0)
              (make-ss-sprite :ss :img-battle-demon-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(0 22) :w 9 :h 7))

(add-resource '(:img :battle :demon :dying-1)
              (make-ss-sprite :ss :img-battle-demon-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(0 14) :w 12 :h 8))

(add-resource '(:img :battle :demon :dying-2)
              (make-ss-sprite :ss :img-battle-demon-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(0 8) :w 14 :h 6))

(add-resource '(:img :battle :demon :dying-3)
              (make-ss-sprite :ss :img-battle-demon-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(0 4) :w 15 :h 4))

(add-resource '(:img :battle :demon :dying-4)
              (make-ss-sprite :ss :img-battle-demon-sprite-sheet
                              :ts *battle-ss-tile-size*
                              :origin '(0 0) :w 16 :h 4))
