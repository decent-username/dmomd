(in-package #:dmomd)

(defclass animatable ()
  ((dungeon-animations
    :accessor dungeon-animations
    :initarg :dungeon-animations
    :initform (list))
   (battle-animations
    :accessor battle-animations
    :initarg :battle-animations
    :initform (list))
   (battle-attack-effect-animations
    :accessor battle-attack-effect-animations
    :initarg :battle-attack-effect-animations
    :initform (list))
   (current-animation
    :accessor current-animation
    :initarg :current-animation
    :initform nil)
   (dungeon-draw-pos
    :accessor dungeon-draw-pos
    :initarg :dungeon-draw-pos
    :initform (tgk:vec2 0 0))
   (battle-draw-pos
    :accessor battle-draw-pos
    :initarg :battle-draw-pos
    :initform (tgk:vec2 0 0)))
  (:documentation "Something that has animations."))


(defmethod get-battle-animation (a &optional (p *player*))
  "return an battle-animation object indexed by the key `a'."
  (cdr (assoc a (battle-animations p))))

(defmethod get-dungeon-animation (a &optional (p *player*))
  "return an dungeon-animation object indexed by the key `a'."
  (cdr (assoc a (dungeon-animations p))))

(defmethod get-animation (a animation-alist)
  "return an battle-animation object indexed by the key `a'."
  (cdr (assoc a animation-alist)))
