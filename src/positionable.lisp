(in-package #:dmomd)

(defclass positionable ()
  ((dungeon-pos
    :accessor dungeon-pos
    :initarg :dungeon-pos
    :initform #(0 0)))
  (:documentation "This class defines the position of an entity in the dungeon."))
