(in-package #:dmomd)


(defclass attack ()
  ((name
    :accessor attack-name
    :initarg  :name
    :initform (error "You need to supply a name for the attack."))
   (type
    :accessor attack-type
    :initarg  :type
    :initform :normal
    :documentation
    "the attack type is a keyword, which can be used to calculate
the damage based on specific vulnerabilities or resistences")
   (sound
    :accessor attack-sound
    :initarg  :sound
    :initform (error "You need to specify a sound file."))
   (damage
    :accessor attack-damage
    :initarg  :damage
    :initform 1)
   (manacost
    :accessor attack-manacost
    :initarg  :manacost
    :initform 0)
   (ability-points-max
    :accessor attack-ability-points-max
    :initarg  :ability-points-max
    :initform -1) ; -1 means infinite attacks
   (ability-points-left
    :accessor attack-ability-points-left
    :initarg  :ability-points-left
    :initform -1); -1 means infinite attacks left
   (sprite
    :accessor attack-sprite
    :initarg :sprite
    :initform nil)
   (callback
    :accessor attack-callback
    :initarg :callback
    :initform (lambda () nil))))

(defmethod print-object ((a attack) out)
  (with-slots (name type)
      a
    (print-unreadable-object (a out)
      (format out "Attack: ~A (~A)" name type))))

(defmethod initialize-instance :after ((a attack) &key)
  "Set ability-points-left to the maximum, because none have been used yet."
  (with-accessors ((ap-max attack-ability-points-max)
                   (ap-left attack-ability-points-left))
      a
    (setf ap-left ap-max)))

(defmacro defatk (atk-name atk-keyword &body body)
  `(setf (gethash ,atk-keyword *attacks*)
         ,`(make-instance 'attack
                          :name ,atk-name
                          ,@body)))

(defmacro make-attack-callbacks (attacker enemies)
  "creates the attack-callbacks for the player"
  (a:with-gensyms (result atk-keyword)
    `(let (,result)
       (dolist (,atk-keyword (attack-keywords ,attacker) (reverse ,result))
         (push
          (lambda ()
            (when (and (can-attack-p ,attacker ,atk-keyword)
                       (enough-mana-p ,attacker ,atk-keyword))
              (attack-enemies ,atk-keyword ,enemies ,attacker)))
          ,result)))))

(defparameter *attack-slash*
  (defatk "slash" :attack-slash
    :type :physical
    :sound :snd-attack-slash
    :damage 3
    :callback
    (lambda () )))

(defparameter *attack-demon-slash*
  (defatk "demon-slash" :attack-demon-slash
    :type :physical
    :sound :snd-attack-demon-slash
    :damage 6
    :callback
    (lambda () )))

(defparameter *attack-fireball*
  (defatk "fireball" :attack-fireball
    :type :fire
    :sound :snd-attack-fireball
    :manacost 3
    :damage 6
    :callback
    (lambda () )))

(defparameter *attack-lightning*
  (defatk "lightning" :attack-lightning
    :type :lightning
    :sound :snd-attack-lightning
    :manacost 2
    :damage 5
    :callback
    (lambda () )))

(defparameter *attack-deny-existence*
  (defatk "deny-existence" :attack-deny-existence
    :type :godlike
    :sound :snd-attack-deny-existence
    :damage :infinity
    :callback
    (lambda () )))

(defparameter *attack-slimeball*
  (defatk "slimeball" :attack-slimeball
    :type :physical
    :sound :snd-attack-slimeball
    :damage 3
    :callback (lambda () )))

(defparameter *attack-bite*
  (defatk "bite" :attack-bite
    :type :physical
    :sound :snd-attack-bite
    :damage 3
    :callback (lambda () )))
