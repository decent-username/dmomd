(in-package #:dmomd)


(defclass sprite ()
  ((image :initarg :image :accessor sprite-image)))

(defun make-sprite (&key image)
  "`image' is a image defined by define-image."
  (make-instance 'sprite :image image))

(defgeneric draw-sprite (s pos))

(defmethod draw-sprite ((s sprite) pos)
  (draw-image pos (sprite-image s)))
