(in-package #:dmomd)

;;;; sprite-sheet sprite  ------------------------------------------------------
(defclass ss-sprite (sprite)
  ;; ss-sprite doesn't need to inherit from sprite.
  ;; I (decent-username) just added it, because I can
  ((origin
    :initarg :origin
    :accessor sprite-origin)
   (width
    :initarg :width
    :accessor sprite-width)
   (height
    :initarg :height
    :accessor sprite-height)))

(defmethod draw-sprite ((s ss-sprite) pos)
  "Draws s at pos"
  (with-accessors ((ss     sprite-image)
                   (origin sprite-origin)
                   (width  sprite-width)
                   (height sprite-height))
      s
    (draw-image pos
                ss
                :origin origin
                :width  width
                :height height)))

(defun make-ss-sprite (&key ss ts origin w h )
  "`ts' is the size of the tiles in the sprite sheet `ss'.
`origin' is a coordinate on the grid define by `ts'."
  (let ((x (elt origin 0))
        (y (elt origin 1)))
    (make-instance 'ss-sprite
                   :image ss
                   :origin (tgk:vec2 (* x ts) (* y ts))
                   :width  (* w ts)
                   :height (* h ts))))
